INKSCAPE_DIR ?= $(shell nitls -pP inkscape_tools)

bin/ld39: $(shell nitls -M src/client/linux_client.nit) gen/solar_assets.nit
	nitc src/client/linux_client.nit -o $@

android: bin/ld39.apk
bin/ld39.apk: $(shell nitls -M src/client/android_client.nit) gen/solar_assets.nit
	nitc src/client/android_client.nit -o $@

bin/ld39_win64.exe: $(shell nitls -M src/client/windows_client.nit) gen/solar_assets.nit
	LDLIBS="-lm -lSDL2_image" CC="ccache /usr/bin/x86_64-w64-mingw32-gcc-posix" NO_STACKTRACE=True \
		nitc src/client/windows_client.nit -o $@

gen/solar_assets.nit: art/solar_assets.svg
	make -C ${INKSCAPE_DIR}
	${INKSCAPE_DIR}/bin/svg_to_png_and_nit -g --src gen/ --scale 2.0 art/solar_assets.svg

check:
	nitunit .

run: bin/ld39
	bin/ld39

clean:
	rm -rf bin/*
