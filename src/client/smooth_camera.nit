import gamnit::flat

import trail

redef class EulerCamera
	# Target position towards which the camera moves smoothly
	var target = new Point3d[Float]

	# Bounding box margin around interesting positions for `target_boxing`
	var boxing_margin = 32.0 is writable

	redef fun reset_height(height)
	do
		super
		target.x = position.x
		target.y = position.y
		target.z = position.z
	end

	fun update_smooth_position(dt: Float)
	do
		var lerp_speed = 0.5*dt
		position.x = lerp_speed.lerp(position.x, target.x)
		position.y = lerp_speed.lerp(position.y, target.y)
		position.z = lerp_speed.lerp(position.z, target.z)
	end

	# Smoothly center and zoom the camera on all `targets`
	fun target_boxing(player_center: Point3d[Float], targets: Point3d[Float]...)
	do
		# Compute bounding box
		var left = player_center.x
		var right = player_center.x
		var top = player_center.y
		var bottom = player_center.y

		for t in targets do
			left = left.min(t.x)
			right = right.max(t.x)
			top = top.max(t.y)
			bottom = bottom.min(t.y)
		end

		# Center on box
		target.x = (left+right)/2.0
		target.y = (bottom+top)/2.0

		# Zoom (set Z) to cover every and a margin
		var margin = boxing_margin*2.0
		var height = top - bottom + margin
		var height_from_width = (right - left + margin) / app.display.as(not null).aspect_ratio
		height = height.max(height_from_width)

		var opp = height / 2.0
		var angle = field_of_view_y / 2.0
		var adj = opp / angle.tan
		target.z = adj
	end
end

redef class App
	redef fun update(dt)
	do
		world_camera.update_smooth_position dt
		super
	end
end
