import client_core

redef class App

	# Dot
	private fun trail_texture: Texture do return app.assets.solar.dot

	private var past_sprites = new Array[Sprite]
	private var max_past_sprites = 32

	private var dt_between_trail = 0.33
	private var cooldown_to_next_trail: Float = dt_between_trail

	private var future_sprites = new Array[Sprite]
	private var max_future_sprites = 64

	# Delta time between each simulated frames
	private var future_time_frame_dt: Float = if on_mobile then 1.0/5.0 else 1.0/10.0

	# Seconds between each future positions kept for the camera
	private var future_positions_period = 1.0
	private var max_future_positions = 12

	private var trail_perf_clock = new Clock is lazy

	fun clear_trail
	do
		past_sprites.clear
		future_sprites.clear
	end

	# Show or update the trail for `ship`
	fun update_trail(dt: Float, ship: Ship, system: GravitySystem)
	do
		trail_perf_clock.lapse

		var sprite_scale = sprite_scale

		# Past sprites
		cooldown_to_next_trail -= dt
		if cooldown_to_next_trail <= 0.0 then
			cooldown_to_next_trail += dt_between_trail

			# New dot
			var sprite = new Sprite(trail_texture, ship.center.offset(0.0, 0.0, -1.0))
			sprite.green = 0.4
			sprite.blue = 0.0
			sprite.alpha = 0.75
			app.sprites.add sprite
			past_sprites.add sprite

			if past_sprites.length > max_past_sprites then
				var over = past_sprites.shift
				app.sprites.remove_all over
			end

			# Update the size of all past sprites
			var i = 0.0
			for s in past_sprites do
				s.scale = sprite_scale * (i/past_sprites.length.to_f).pow(0.25)
				i += 1.0
			end
		end
		perfs["client trail past"].add trail_perf_clock.lapse

		# Future sprites

		## Simulated ship
		var ghost_ship = new Ship(ship.mass, ship.radius)
		ghost_ship.center.x = ship.center.x
		ghost_ship.center.y = ship.center.y
		ghost_ship.velocity.x = ship.velocity.x
		ghost_ship.velocity.y = ship.velocity.y

		# Position of `ghost_ship` at the last iteration
		var last_pos = new Point3d[Float](ghost_ship.center.x, ghost_ship.center.y)

		# Reset the future positions
		ship.future_positions.clear

		## Simulate `future_time_max` seconds
		var si = 0
		dt = future_time_frame_dt
		var cooldown_to_next_trail_future = cooldown_to_next_trail
		var cooldown_to_next_future_position = 0.0
		loop
			# Simulate
			system.update_single(dt, ghost_ship, ship)

			# Place trail
			cooldown_to_next_trail_future -= dt
			if cooldown_to_next_trail_future <= 0.0 then
				var p = -cooldown_to_next_trail_future/dt
				cooldown_to_next_trail_future += dt_between_trail

				# Add sprite
				var s
				if future_sprites.length > si then
					s = future_sprites[si]
				else
					s = new Sprite(trail_texture, ghost_ship.center.offset(0.0, 0.0, -1.0))
					s.red = 0.0
					s.green = 0.75
					s.alpha = 0.75
					app.sprites.add s
					future_sprites.add s
				end

				var scale_dist_mod = (max_future_sprites.to_f-si.to_f)/max_future_sprites.to_f
				scale_dist_mod = scale_dist_mod.pow(0.25)
				s.scale = sprite_scale * scale_dist_mod
				si += 1

				s.center.x = p.lerp(ghost_ship.center.x, last_pos.x)
				s.center.y = p.lerp(ghost_ship.center.y, last_pos.y)

				if si >= max_future_sprites then break
			end

			last_pos.x = ghost_ship.center.x
			last_pos.y = ghost_ship.center.y

			# Keep some future position to correctly center the camera
			cooldown_to_next_future_position -= dt
			if cooldown_to_next_future_position <= 0.0 then
				cooldown_to_next_future_position += future_positions_period

				if ship.future_positions.length > max_future_positions then continue
				ship.future_positions.add ghost_ship.center.offset(0.0, 0.0, 0.0)
			end
		end

		perfs["client trail future"].add trail_perf_clock.lapse
	end

	# Mod for `Sprite::scale` for the current camera position
	private fun sprite_scale: Float
	do
		# close at 200.0, far at 2200.0
		var cam_dist = app.world_camera.position.z

		var target_screen_height = 2.0 + (cam_dist-200.0) * 10.0 / 2000.0

		return target_screen_height / trail_texture.width
	end

	# Hide the last trail shown
	fun hide_trail
	do
		for s in past_sprites do app.sprites.remove_all s
		past_sprites.clear

		for s in future_sprites do app.sprites.remove_all s
		future_sprites.clear
	end
end

redef class Ship
	# Some future positions
	var future_positions = new Array[Point3d[Float]]
end

redef class GravitySystem
	# Duplicated from `PhysicsSystem::update` for a sigle object `o1`
	private fun update_single(dt: Float, o1, ignore: PhysicsObject)
	do
		# Rebound
		for o2 in objects do
			if o1 != o2 and o2 != ignore then
				if sphere_collide(o1.center, o2.center, o1.radius, o2.radius) then

					# Cut the trail here
					return
				end
			end
		end

		var force = new Point3d[Float]
		for o2 in objects do
			if o1 != o2 and o2 != ignore then
				var dir = o2.center - o1.center
				dir.normalize
				force += dir * force_between_objects(o1, o2)
			end
		end
		o1.velocity.x += force.x / o1.mass * dt
		o1.velocity.y += force.y / o1.mass * dt

		# Update positions
		o1.center.x += o1.velocity.x * dt
		o1.center.y += o1.velocity.y * dt
	end
end
