import client_core
import play_view

class DeathView
	super View

	private var old_view: PlayView

	redef fun create
	do
		var t = old_view.time_elapsed
		var text = "You survived for {t.pretty_time}!"

		if t > 30.0 * 60.0 then
			text += "\nThat's impressive!"
		else if t > 10.0 * 60.0 then
			text += "\nWell played!"
		else if t > 1.0 * 60.0 then
			text += "\ngg"
		else
			text += "\nCome on..."
		end

		text += "\nPress enter to play again"
		print text

		app.status_text.text = text
	end

	redef fun destroy
	do
		app.status_text.text = null
		old_view.destroy
	end

	redef fun accept_event(event)
	do
		if (event isa KeyEvent and not event.is_down and event.name == "return") or
		   (event isa PointerEvent and event.depressed and not event.is_move) then
			destroy
			app.start_new_game
			return true
		end

		return false
	end
end

redef class Float
	fun pretty_time: Text
	do
		var minutes = to_i / 60
		var seconds = to_i % 60
		var tenths = (self * 10.0).to_i % 10
		return if minutes > 0 then
			"{minutes} minutes and {seconds}.{tenths} seconds"
		else "{seconds}.{tenths} seconds"
	end
end
