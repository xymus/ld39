import gamnit::depth

import play_view

redef class Assets
	var ship_model = new Model("models/ship.obj")
end

redef class PlayView
	redef fun create
	do
		super
		gl.capabilities.cull_face.enable
	end

	redef fun enter_system(system)
	do
		super

		app.light.position.x = system.center.x
		app.light.position.y = system.center.y
		app.light.position.z = system.center.z
	end

	redef fun destroy
	do
		super
		app.actors.clear
	end
end

redef class PhysicsObject

	var actor: nullable Actor = null

	redef fun remove_from_scene
	do
		var actor = actor
		if actor == null then return
		app.actors.remove actor
		self.actor = null
	end

	private fun add_to_scene_as_sphere(ambient_color, diffuse_color, specular_color: Array[Float])
	do
		var mesh = new UVSphere(radius, 8, 5)
		var material = new SmoothMaterial(ambient_color, diffuse_color, specular_color)
		var model = new LeafModel(mesh, material)
		var actor = new Actor(model, center)
		self.actor = actor
		app.actors.add actor
	end

	private fun add_to_scene_as_cube(ambient_color, diffuse_color, specular_color: Array[Float])
	do
		var mesh = new Cube
		var material = new SmoothMaterial(ambient_color, diffuse_color, specular_color)
		var model = new LeafModel(mesh, material)
		var actor = new Actor(model, center)
		actor.scale = radius * 1.5
		self.actor = actor
		app.actors.add actor

		actor.yaw = 2.0 * pi.rand
		actor.roll = 2.0 * pi.rand
		actor.pitch = 2.0 * pi.rand
	end
end

redef class Star

	redef fun add_to_scene
	do add_to_scene_as_sphere([1.0, 0.9, 0.5, 1.0],
	                          [0.0]*4,
							  [0.0]*4)
end

redef class SolarSystem
	redef fun update_supernova(dt)
	do
		super

		var p = 1.0 - (t_to_supernova / supernova_period)
		for star in stars do
			var actor = star.actor
			if actor != null then
				var s = if p < 0.8 then
					(p/0.8).qerp(1.0, 0.2, 0.2)
				else ((p-0.8)/0.2).pow(4.0).lerp(0.2, self.radius/star.radius)
				actor.scale = s
				#print "{p}  lerp {star.radius} {self.radius}"

				var g = p.qerp(0.9, -0.5, 0.45)
				var b = p.qerp(0.5, -0.5, 0.25)
				actor.model.leaves.first.material.as(SmoothMaterial).ambient_color[1] = g
				actor.model.leaves.first.material.as(SmoothMaterial).ambient_color[2] = b
			end
		end
	end
end

redef class Planet

	redef fun add_to_scene
	do
		var r = 1.0.rand
		var g = 1.0.rand
		var b = 1.0.rand
		add_to_scene_as_sphere([0.2*r, 0.2*b, 0.2*g, 1.0],
	                           [0.6*r, 0.6*g, 0.6*b, 1.0],
	                           [0.2, 0.2, 0.2, 0.2])
	end
end

redef class Comet

	redef fun add_to_scene
	do add_to_scene_as_sphere([0.0, 0.1, 0.1, 1.0],
	                          [0.0, 0.8, 0.8, 1.0],
	                          [1.0, 1.0, 1.0, 0.2])
end

redef class Asteroid

	redef fun add_to_scene
	do
		var w = 0.25 + 0.25.rand
		add_to_scene_as_cube([0.2, 0.2, 0.2, 1.0],
	                         [w, w, w, 1.0],
	                         [1.0, 1.0, 1.0, 0.1])
	end
end

redef class Ship

	private var actors = new Array[Actor]

	private var actor_power: nullable Actor = null
	private var actor_thrust_left: nullable Actor = null
	private var actor_thrust_right: nullable Actor = null
	private var actor_thrust_boost: nullable Actor = null
	private var actor_sail: nullable Actor = null
	private var actor_shield: nullable Actor = null

	private fun scale: Float do return radius * 2.0

	redef fun add_to_scene
	do
		# 0 power, 1 sail, 2 right, 3 left, 4 boost, 5 hull, 6 power_back
		var model = app.assets.ship_model

		var actor = new Actor(model.leaves[5], center)
		self.actor = actor
		actors.add actor

		actor = new Actor(model.leaves[6], center)
		actors.add actor

		actor = new Actor(model.leaves[0], center)
		self.actor_power = actor
		actors.add actor

		actor = new Actor(model.leaves[2], center)
		self.actor_thrust_right = actor
		actors.add actor

		actor = new Actor(model.leaves[3], center)
		self.actor_thrust_left = actor
		actors.add actor

		actor = new Actor(model.leaves[4], center)
		self.actor_thrust_boost = actor
		actors.add actor

		actor = new Actor(model.leaves[1], center)
		self.actor_sail = actor
		actors.add actor

		var sphere = new UVSphere(1.0, 8, 5)
		var blue =  [0.0*0.0, 1.0*0.1, 1.0*0.1, 0.1]
		var blue1 = [1.0*0.0, 1.0*0.2, 1.0*0.3, 0.1]
		var spec =  [1.0,         1.0,     1.0, 0.1]
		var shield_material = new SmoothMaterial(blue, blue1, spec)
		var shield_model = new LeafModel(sphere, shield_material)
		actor = new Actor(shield_model, center)
		self.actor_shield = actor
		actors.add actor

		for a in actors do
			a.scale = scale
			a.pitch = pi/2.0
			app.actors.add a
		end
	end

	redef fun update_scene(dt)
	do
		super

		for actor in actors do
			actor.roll = -heading + pi/2.0
		end

		# Max thrust variation
		var v = 1.0

		actor_thrust_boost.as(not null).scale = if
			propulsion <= 0.0 then 0.0 else scale + v.rand
		actor_thrust_left.as(not null).scale = if
			angular_acceleration >= 0.0 then 0.0 else scale + v.rand
		actor_thrust_right.as(not null).scale = if
			angular_acceleration <= 0.0 then 0.0 else scale + v.rand

		actor_power.as(not null).scale = scale * power

		# Sail
		var target = if open_sail then scale*1.5 else 0.0
		actor_sail.as(not null).scale = (0.9*dt).lerp(actor_sail.as(not null).scale, target)

		# Shield
		#actor_shield.as(not null).alpha = shield
		actor_shield.as(not null).scale = shield * scale * 1.5
	end
end
