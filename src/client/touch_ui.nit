intrude import gamnit::virtual_gamepad # FIXME App::gamepad

import play_view

redef class PlayView
	redef fun create
	do
		super

		var gamepad = new VirtualGamepad
		gamepad.add_dpad
		gamepad.visible = true
		app.gamepad = gamepad
	end
end
