import client_core

intrude import verse

redef class Game
	private var star_per_x = new MultiHashMap[Int, Sprite]

	redef fun populate_grid_case(grid_x, grid_y)
	do
		var center_x = grid_case_size * grid_x
		var center_y = grid_case_size * grid_y

		# Add stars
		var n_stars = 5000
		for i in [0..n_stars[ do
			var star_center = new Point3d[Float](
				center_x - grid_case_size/2.0 + grid_case_size.rand,
				center_y - grid_case_size/2.0 + grid_case_size.rand,
				-1000.0 - 500.0.rand)

			if star_center.x < wave_border then continue

			var s = new Sprite(app.assets.solar.star.rand, star_center)
			s.scale = 0.5 + 1.0.rand
			s.draw_order = -100
			app.sprites.add s

			star_per_x[star_center.x.a].add s
		end

		return super
	end

	redef fun wave_advance(from, to)
	do
		for x in [from.a .. to.a[ do
			var stars = star_per_x[x]
			for star in stars do
				star.green = 0.0
				star.blue = 0.0
			end
		end

		var remove_dist = 1500.0
		for x in [(from-remove_dist).a .. (to-remove_dist).a[ do
			var stars = star_per_x[x]
			for star in stars do app.sprites.remove star
			star_per_x.keys.remove(x)
		end

		for x in [(from-remove_dist).a+1 .. (to).a[ do
			var stars = star_per_x[x]
			for star in stars do
				var p = (to-star.center.x) / remove_dist

				var s = if p < 0.8 then
					(p/0.8).lerp(1.0, 0.6)
				else ((p-0.8)/0.2).pow(4.0).lerp(0.6, 8.0)
				star.scale = s
			end
		end
	end
end

redef class Float
	private fun a: Int do return (self/10.0).to_i
end
