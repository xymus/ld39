import client_core

redef class App

	# Splash screen, shown while loading
	var splash_texture = new Texture("textures/splash.png")
end

class SplashView
	super View

	private var splash: nullable Sprite = null

	redef fun create
	do
		# Bring back the splash screen
		var splash = new Sprite(app.splash_texture, app.ui_camera.center.offset(0.0, 0.0, 0.0))
		self.splash = splash
		app.ui_sprites.add splash

		app.status_text.text = "Press any key to start"

		# To recreate the splash screen
		#status_text.text = "Spacious\nRighteous\nStarship"
	end

	redef fun destroy
	do
		var splash = splash
		if splash != null then
			app.ui_sprites.remove splash
			self.splash = null
		end
	end

	redef fun accept_event(event)
	do
		if (event isa KeyEvent and not event.is_down) or
		   (event isa PointerEvent and event.depressed and not event.is_move) then
			app.start_new_game
			return true
		end

		return false
	end
end
