intrude import gamnit::font # For TextSprites::font=

import client_core
import trail
import smooth_camera
import background

redef class App
	private fun alert(text: Text, danger: Bool)
	do
		alert_text.font = if danger then alert_font else big_font
		alert_text.text = text
		alert_time_to_live = 4.0
	end

	private var alert_text = new TextSprites(big_font, ui_camera.top.offset(0.0, -64.0+top_offset, 0.0), align=0.5) is lazy
	private var alert_time_to_live = 0.0

	private fun update_alert_text(dt: Float)
	do
		if alert_time_to_live == 0.0 then return

		alert_time_to_live -= dt
		if alert_time_to_live <= 0.0 then
			alert_time_to_live = 0.0
			alert_text.text = null
		end
	end
end

class PlayView
	super View

	# Current game, larger context than a single `system`
	var game: Game

	# Current solar system (close up)
	var system: nullable GravitySystem

	# Play speed modifier, > 1.0 for faster play
	var time_mod = 1.0

	# Time elapsed
	var time_elapsed = 0.0

	private var power_anchor: Point3d[Float] = app.ui_camera.top.offset(-256.0, -50.0+top_offset, 0.0)
	private var power_sprite = new Sprite(app.assets.solar.power_icon, power_anchor.offset(-32.0, 0.0, 0.0))
	private var power_bar = new PowerBar(400.0, 40.0, power_anchor)

	private var shield_anchor: Point3d[Float] = app.ui_camera.top.offset(256.0, -50.0+top_offset, 0.0)
	private var shield_sprite = new Sprite(app.assets.solar.shield_icon, shield_anchor.offset(-32.0, 0.0, 0.0))
	private var shield_bar = new ShieldBar(100.0, 40.0, shield_anchor) is lazy

	private var update_perf_clock = new Clock is lazy

	redef fun create
	do
		power_bar.create
		app.ui_sprites.add power_sprite
		power_bar.turn_on power_sprite

		shield_bar.create
		app.ui_sprites.add shield_sprite
		shield_bar.turn_on shield_sprite
	end

	redef fun update(dt)
	do
		update_perf_clock.lapse

		var world_dt = dt * time_mod

		var steering = 0.0
		var boost = 0.0
		var open_sail = false
		for key in app.pressed_keys do
			if key == "left" or key == "a" then
				steering += 1.0
			else if key == "right" or key == "d" then
				steering += -1.0
			else if key == "up" or key == "w" then
				boost = 1.0
			else if key == "down" or key == "s" then
				open_sail = true
			end
		end
		steering = steering.clamp(-1.0, 1.0)

		game.player_ship.steering = steering
		game.player_ship.boost = boost
		game.player_ship.open_sail = open_sail
		perfs["client update input"].add update_perf_clock.lapse

		var system = system
		if system == null then system = game.verse_system

		game.update_wave world_dt

		# Update game logic for the current system
		system.update world_dt
		perfs["client update physics"].add update_perf_clock.lapse

		# Update ship's power
		game.player_ship.update_power(world_dt, system)
		app.update_alert_text dt

		power_bar.value = game.player_ship.power
		shield_bar.value = game.player_ship.shield
		perfs["client update power"].add update_perf_clock.lapse

		# Update timer
		time_elapsed += world_dt
		var minutes = time_elapsed.to_i / 60
		var seconds = time_elapsed.to_i % 60
		app.minutes_text.text = "{if minutes < 10 then "0" else ""}{minutes}"
		app.seconds_text.text = "{if seconds < 10 then "0" else ""}{seconds}"
		app.tenths_text.text = ((time_elapsed * 10.0).to_i % 10).to_s
		app.dot_seconds_text.text = ":"
		app.dot_tenths_text.text = "."

		# Transitions
		if system isa SolarSystem and not system.contains(game.player_ship.center) then
			# Exit
			exit_system
		else if system isa VerseSystem then
			# Enter
			# TODO something much more perfomant
			for s in game.solar_systems do
				if s.contains(game.player_ship.center) then
					enter_system s
					break
				end
			end
		end
		perfs["client update transition"].add update_perf_clock.lapse

		if system isa VerseSystem then
			# Generate solar systems
			game.explore game.player_ship.center
		end
		perfs["client update explore"].add update_perf_clock.lapse

		# Zoom/center camera on the ship and its future positions
		var positions_of_interest = game.player_ship.future_positions
		if system isa SolarSystem then
			for star in system.stars do
				positions_of_interest.add star.center
			end
			app.world_camera.boxing_margin = 64.0
		else if system isa VerseSystem then
			# add the two closest stars? TODO
			app.world_camera.boxing_margin = 1600.0
		end
		app.world_camera.target_boxing(game.player_ship.center, positions_of_interest...)
		perfs["client update cam_box"].add update_perf_clock.lapse

		app.update_trail(world_dt, game.player_ship, system)
		perfs["client update trail"].add update_perf_clock.lapse
	end

	redef fun destroy
	do
		power_bar.destroy
		#app.sprites.remove power_sprite
		shield_bar.destroy
		#app.sprites.remove shield_sprite

		app.alert_text.text = null

		app.sprites.clear
		app.ui_sprites.clear

		app.dot_seconds_text.text = null
		app.dot_tenths_text.text = null

		app.clear_trail
	end

	# Switch to the close up view of `system` only
	fun enter_system(system: SolarSystem)
	do
		self.system = system
		system.add_to_scene_solar
		system.objects.add game.player_ship

		time_mod = 1.0
	end

	# Switch to the far away view, leaving the close system view
	fun exit_system
	do
		var system = system
		if system != null then
			system.remove_from_scene
			system.objects.remove_all game.player_ship
		end

		self.system = null

		time_mod = 4.0
	end
end

class Bar
	var width: Float
	var height: Float

	var anchor: Point3d[Float]

	var sprites = new Array[Sprite]

	fun create
	do
		var texture = new CustomTexture(1.0, height)
		texture.fill([1.0]*4)
		texture.load

		for i in [0..width.to_i[ do
			var s = new Sprite(texture, anchor.offset(i.to_f, 0.0, 0.0))
			s.red = 0.2
			s.green = 0.2
			s.blue = 0.2
			sprites.add s
			app.ui_sprites.add s
		end
	end

	fun destroy
	do
		for s in sprites do app.ui_sprites.remove s
		sprites.clear
	end

	var prev_value = 0.0
	fun value=(value: Float)
	do
		var p = prev_value*sprites.length.to_f
		var n = value*sprites.length.to_f
		if p == n then return

		for i in [p.min(n).to_i .. p.max(n).to_i[ do
			var s = sprites[i]
			if p > n then
				# Turning off
				s.red = 0.2
				s.green = 0.2
				s.blue = 0.2
			else
				turn_on s
			end
		end
		prev_value = value
	end

	private fun turn_on(sprite: Sprite) do end
end

private class PowerBar
	super Bar

	redef fun turn_on(sprite)
	do
		sprite.red = 0.0
		sprite.green = 0.8
		sprite.blue = 0.0
	end
end

private class ShieldBar
	super Bar

	redef fun turn_on(sprite)
	do
		sprite.red = 0.0
		sprite.green = 0.4
		sprite.blue = 1.0
	end
end

redef class GravitySystem
	redef fun update(dt)
	do
		super

		# Update UI
		for object in objects do object.update_scene(dt)
	end

	# Add objects `seen_from_verse` to the UI
	fun add_to_scene_verse
	do
		for object in objects do
			if object.seen_from_verse then object.add_to_scene
		end
	end

	# Add local objects to the UI
	fun add_to_scene_solar
	do
		for object in objects do
			if not object.seen_from_verse then object.add_to_scene
		end
	end

	# Remove local objects from the UI
	fun remove_from_scene
	do
		for object in objects do
			if not object.seen_from_verse then object.remove_from_scene
		end
	end
end

redef class SolarSystem

	var dots = new Array[Sprite]

	redef fun add_to_scene_solar
	do
		super
		var num_dots = 64
		for i in [0..num_dots[ do
			var angle = 2.0 * i.to_f * pi / num_dots.to_f
			var sprite = new Sprite(app.assets.solar.dot, limit_at_angle(angle))
			sprite.scale *= 0.3
			sprite.alpha *= 1.0
			dots.add sprite
			app.sprites.add sprite
		end
	end

	redef fun remove_from_scene
	do
		super
		for dot in dots do
			app.sprites.remove dot
		end
		dots.clear
	end

	redef fun end_supernova
	do
		super
		app.assets.star_explosion_sound.play
		remove_from_scene
	end
end

redef class PhysicsObject

	var sprite = new Sprite(texture, center) is lazy

	fun texture: Texture do return new CheckerTexture

	# Add to the visible world (`app.sprites`)
	fun add_to_scene
	do
		sprite.scale = 2.0 * radius / texture.height
		app.sprites.add sprite
	end

	# Remove from the visible world (`app.sprites`)
	fun remove_from_scene
	do
		app.sprites.remove_all sprite
	end

	# Update in the visible world
	fun update_scene(dt: Float) do end

	# Is `self` visible from the verse view? (far away)
	fun seen_from_verse: Bool do return false

	redef fun destroy do remove_from_scene
end

redef class Ship

	var jetpack_cooldown = 0.0

	redef fun texture do return app.assets.ship

	redef fun update_scene(dt)
	do
		super
		if jetpack_cooldown <= 0.0 then
			if propulsion > 0.0 then
				app.assets.jetpack_propulsion_sound.play
				jetpack_cooldown = 0.2
			else if angular_acceleration != 0.0 then
				app.assets.jetpack_steering_sound.play
				jetpack_cooldown = 0.2
			end
		end
		jetpack_cooldown -= dt
		sprite.rotation = heading - pi/2.0
	end

	redef fun seen_from_verse do return true

	redef fun alert_running_out_of_power do app.alert("Running out of power!", true)
	redef fun alert_out_of_power do app.alert("All out of power!", true)
	redef fun alert_full_of_power do app.alert("At full power!", false)
	redef fun alert_half_power do app.alert("Half power", false)

	redef fun rebound(other)
	do
		app.assets.collision_sound.play
		return super
	end
end

redef class Star
	redef fun texture do return app.assets.solar.large_star
	redef fun seen_from_verse do return true
end

redef class Planet
	redef fun texture do return app.assets.solar.planet
end

redef class Asteroid
	redef fun texture do return app.assets.solar.asteroid
end

redef class Comet
	redef fun texture do return app.assets.solar.comet

	redef fun update_scene(dt)
	do
		super

		var system = system
		if system != null then
			sprite.rotation = center.atan2(system.center)+pi
		end
	end
end

redef class Game
	redef fun register_system(solar_system)
	do
		super
		solar_system.add_to_scene_verse
	end
end
