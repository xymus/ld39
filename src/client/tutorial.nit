import play_view

redef class PlayView

	private var tutorial_texts = new Array[TextSprites] is lazy

	private fun show_tutorial_text
	do
		var texts = tutorial_texts

		var line_height = app.big_font.desc.line_height
		for i in 4.times do
			# FIXME use ui_camera.left
			var pos = app.ui_camera.center.offset(-app.display.as(not null).width.to_f/2.0 + 32.0,
			                                      -i.to_f*line_height + 128.0 + tutorial_offset, 0.0)
			var ts = new TextSprites(app.big_font, pos)
			ts.scale = 0.75
			texts.add ts
		end

		texts[0].text = "Steer with [a] and [d]"
		texts[1].text = "Boost with [w]"
		texts[2].text = "Slow down with [s]"
		texts[3].text = "\nLeave the gravity well\nand head right, always right,\nto flee the wave of dark matter.\nSurvive as long as possible!"

		for text in texts do
			for name, link in text.links do
				for sprite in link do sprite.red = 0.0
			end
		end
	end

	fun tutorial_offset: Float do return 0.0

	private fun update_tutorial_text
	do
		for key in app.pressed_keys do
			if key == "a" or key == "d" or key == "left" or key == "right" then
				tutorial_texts[0].text = null
			else if key == "w" or key == "up" then
				tutorial_texts[1].text = null
			else if key == "s" or key == "down" then
				tutorial_texts[2].text = null
			end
		end
	end

	redef fun update(dt)
	do
		if tutorial_texts.not_empty then update_tutorial_text
		super
	end

	redef fun create
	do
		super
		show_tutorial_text
	end

	redef fun exit_system
	do
		super
		tutorial_texts[3].text = null
	end
end

redef class Ship
	redef fun destroy
	do
		var view = app.view
		if view isa PlayView then
			for t in view.tutorial_texts do t.text = null
		end

		super
	end
end
