import gamnit::flat

import assets

import game

redef class App

	fun view: nullable View do return null

	# Client assets: textures, etc.
	var assets = new Assets

	# General big font
	var big_font = new BMFontAsset("fonts/righteous/font.fnt")

	# Alert font
	var alert_font = new BMFontAsset("fonts/righteous_red/font.fnt")

	# Text at the bottom of the screen, for directions or other feedback
	var status_text = new TextSprites(big_font, ui_camera.bottom.offset(0.0, 64.0, 0.0), align=0.5, valign=1.0) is lazy

	# Timer
	var minutes_text     = new TextSprites(big_font, ui_camera.top_right.offset(-260.0, -128.0+top_offset, 0.0), align=1.0, valign=1.0) is lazy
	var dot_seconds_text = new TextSprites(big_font, ui_camera.top_right.offset(-240.0, -128.0+top_offset, 0.0), align=1.0, valign=1.0) is lazy
	var seconds_text     = new TextSprites(big_font, ui_camera.top_right.offset(-150.0, -128.0+top_offset, 0.0), align=1.0, valign=1.0) is lazy
	var dot_tenths_text  = new TextSprites(big_font, ui_camera.top_right.offset(-130.0, -128.0+top_offset, 0.0), align=1.0, valign=1.0) is lazy
	var tenths_text      = new TextSprites(big_font, ui_camera.top_right.offset( -85.0, -128.0+top_offset, 0.0), align=1.0, valign=1.0) is lazy

	# Setup a whole new game, the first one or after death
	fun start_new_game do end
end

# Window or otherwise full screen view context
class View

	# Create and show this view
	fun create do end

	# Hide and destroy this view
	fun destroy do end

	# Update at each frame for `dt` secodns
	fun update(dt: Float) do end

	# Apply `event`, return true if it is intercepted
	fun accept_event(event: InputEvent): Bool do return false
end

fun top_offset: Float do return if on_mobile then -64.0 else 0.0
