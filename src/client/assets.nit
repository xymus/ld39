import gamnit::flat

import gen::solar_assets

class Assets
	var ship = new Texture("textures/ship.png")

	var solar = new Solar_AssetsImages

	var collision_sound = new Sound("sounds/explosion_ship.wav")

	var star_explosion_sound = new Sound("sounds/explosion_asteroids.wav")

	var jetpack_propulsion_sound = new Sound("sounds/jetpack.ogg")

	var jetpack_steering_sound = new Sound("sounds/jetpack_low.ogg")
end
