import more_collections

import physic_system
import solar

redef class Game

	# The whole universe physics system
	var verse_system = new VerseSystem

	# All generated `SolarSystem`
	var solar_systems = new Array[SolarSystem]

	init do verse_system.objects.add player_ship

	# Add a `solar_system`
	protected fun register_system(solar_system: SolarSystem)
	do
		solar_systems.add solar_system
		for o in solar_system.objects do if o isa Star then verse_system.objects.add o
		#print "register {solar_system.center}"
	end

	fun wave_border: Float do return 0.0

	# ---
	# Grid for procedural solar system generation

	private var grid_case_size = 10000.0

	private var grid = new HashMap2[Float, Float, Bool]

	fun explore(center: Point3d[Float])
	do
		var d = 4000.0
		for x in [-1.0, 1.0] do
			for y in [-1.0, 1.0] do
				explore_corner center.offset(d*x, d*y, 0.0)
			end
		end
	end

	private fun explore_corner(center: Point3d[Float])
	do
		var grid_x = (center.x / grid_case_size).round
		var grid_y = (center.y / grid_case_size).round
		if grid[grid_x, grid_y] == true then return

		populate_grid_case(grid_x, grid_y)
	end

	private var verse_perf_clock = new Clock is lazy

	private fun populate_grid_case(grid_x, grid_y: Float): Array[SolarSystem]
	do
		verse_perf_clock.lapse

		var center_x = grid_case_size * grid_x
		var center_y = grid_case_size * grid_y

		var margin = 500.0
		var gen_range = grid_case_size - margin*2.0

		var systems = new Array[SolarSystem]
		var n_systems = 12 + 12.rand

		var tries = 0
		while systems.length < n_systems do
			tries += 1
			if tries > 100 then
				print "Warning: Giving up at {systems.length+1}th system (target {n_systems})"
				break
			end

			var prop_center = new Point3d[Float](
				center_x - gen_range/2.0 + gen_range.rand,
				center_y - gen_range/2.0 + gen_range.rand
				)

			# Force the first system to start at 100.0
			if self.solar_systems.is_empty then
				prop_center = new Point3d[Float](0.0, 0.0)
			end

			if prop_center.x < wave_border then
				continue
			end

			for system in systems do
				var d =  prop_center.dist(system.center)
				if d < margin * 3.0 then # TODO better detect systems too close
					continue label
				end
			end

			var system = generate_system(prop_center)
			register_system system
			systems.add system

		end label

		grid[grid_x, grid_y] = true
		perfs["game populate"].add verse_perf_clock.lapse

		return systems
	end
end

# Whole universe, containing only stars and the player's ship
class VerseSystem
	super GravitySystem
end
