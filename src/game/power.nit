import game_core
import physic_system
import solar

redef class Ship
	# Proportion of power accumulated (out of 1.0)
	var power = 1.0

	# Is the power collecting sail open?
	var open_sail = false is writable

	redef fun propulsion do return if open_sail then
			super - 0.1
		else super

	redef fun steering_mod do return if power > 0.0 then 1.0 else 0.0

	redef fun boost_mod do return if power > 0.0 then 1.0 else 0.0

	fun update_power(dt: Float, system: GravitySystem)
	do
		var e = power

		# Gain power when close to a star
		var solar_power = 0.0
		if system isa SolarSystem then for o in system.objects do
			var max_dist_for_power = system.radius
			if o isa Star then
				var dist = center.dist(o.center)
				var inv_dist = (max_dist_for_power - dist)/max_dist_for_power

				if inv_dist < 0.0 then continue

				solar_power += inv_dist.pow(0.75) * 0.04 * dt
				#print "+ {inv_dist * 0.01 * dt}"
			end
		end

		# The sail boost solar_power
		if open_sail then solar_power *= 5.0

		e += solar_power

		# Life support consumes power
		e -= 0.0002 * dt

		if steering != 0.0 then
			# Turning consumes power
			e -= 0.05*dt
			#print "- {0.1*dt}"
		end

		if boost != 0.0 then
			# Boosting consumes power
			e -= 0.1*dt
		end

		var new_power = e.clamp(0.0, 1.0)

		var overflow = e - new_power
		if overflow >= 0.0 then power_overflow(overflow)

		# Alert the user
		if new_power == 1.0 and power != 1.0 then
			alert_full_of_power
		else if new_power == 0.0 and power != 0.0 then
			alert_out_of_power
		else if new_power <= 0.2 and power > 0.2 then
			alert_running_out_of_power
		else if (new_power <= 0.5 and power > 0.5) or (new_power > 0.5 and power <= 0.5) then
			alert_half_power
		end

		#print e
		power = new_power
	end

	fun power_overflow(over: Float) do end

	fun alert_running_out_of_power do end
	fun alert_out_of_power do end
	fun alert_full_of_power do end
	fun alert_half_power do end
end
