import geometry
import performance_analysis
import lib

# An object responding to or causing gravity
class PhysicsObject

	# Gravity center
	var center = new Point3d[Float]

	# Velocity
	var velocity = new Point3d[Float]

	# Mass
	var mass: Float

	# Approximate radius
	var radius: Float

	# Angular speed
	fun angular_acceleration: Float do return 0.0

	# Self propulsion towards `heading`
	fun propulsion: Float do return 0.0

	# If true, the object is not attracted to others
	fun pinned: Bool do return false

	# Destroys this physic object
	fun destroy do end
end

# Space ship
class Ship
	super PhysicsObject

	# Current orientation, right hand rule, 0.0 pointing right
	fun heading: Float
	do
		return atan2(velocity.y, velocity.x)
	end

	# Steer this ship, following the right hand rule
	#
	# * = 0.0 to go straight
	# * > 0.0 for CCW
	# * < 0.0 for CW
	#
	# Values should be clamped in [-1.0 .. 1.0].
	var steering = 0.0 is writable

	# Boost speed
	#
	# Values should be clamped in [-1.0 .. 1.0].
	var boost = 0.0 is writable

	redef fun angular_acceleration
	do
		return pi * steering * 0.05 * steering_mod
	end

	# Modifier of the `angular_acceleration` effect of `steering`, affected by power levels
	fun steering_mod: Float do return 1.0

	redef fun propulsion do return boost * boost_mod * 0.025

	# Modifier of the `proportion` effect of `boost`, affected by power levels
	fun boost_mod: Float do return 1.0
end

class Star
	super PhysicsObject
end

class PinnedStar
	super Star

	redef fun pinned
	do
		return true
	end
end

class Game
	# Controllable ship
	var player_ship: Ship
end

fun on_mobile: Bool
do
	return false
end
