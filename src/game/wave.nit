import solar
import verse

redef class Game

	# Starting distance from the player
	var wave_starting_dist = 2000.0

	# Wave speed per seconds
	var wave_speed = 15.0

	redef var wave_border = player_ship.center.x - wave_starting_dist is lazy

	var supernova_systems = new List[SolarSystem]

	fun update_wave(dt: Float)
	do
		var old_border = wave_border
		wave_border += wave_speed*dt

		for solar in solar_systems do
			var x = solar.center.x
			if old_border <= x and x < wave_border then
				solar.begin_supernova
				supernova_systems.add solar
			end
		end

		wave_advance(old_border, wave_border)

		for solar in supernova_systems.reverse_iterator do
			solar.update_supernova dt
			if solar.t_to_supernova <= 0.0 then
				# Remove all references to this system
				supernova_systems.remove solar
				solar_systems.remove solar
				for s in solar.stars do verse_system.objects.remove(s)
			end
		end
	end

	fun wave_advance(from, to: Float)
	do
	end
end

redef class SolarSystem

	var t_to_supernova = 0.0
	var supernova_period = 20.0

	fun begin_supernova
	do
		t_to_supernova = supernova_period
	end

	fun update_supernova(dt: Float)
	do
		t_to_supernova -= dt
		if t_to_supernova <= 0.0 then
			# explode
			end_supernova
		end
	end

	fun end_supernova
	do
		for object in objects do object.destroy
	end
end
