import geometry

redef class Point3d[N]

	fun length2: Float
	do
		assert self isa Point3d[Float]
		return x * x + y * y
	end

	fun length: Float
	do
		assert self isa Point3d[Float]
		return length2.sqrt
	end

	fun normalize
	do
		assert self isa Point3d[Float]
		var l = self.length
		assert l != 0
		x /= l
		y /= l
	end

	fun set_length(length: Float)
	do
		assert self isa Point3d[Float]
		normalize
		x *= length
		y *= length
	end

	fun +(other: Point3d[N]): Point3d[N]
	do
		assert self isa Point3d[Float]
		assert other isa Point3d[Float]
		var result = new Point3d[Float]
		result.x = self.x + other.x
		result.y = self.y + other.y
		return result
	end

	fun -(other: Point3d[N]): Point3d[N]
	do
		assert self isa Point3d[Float]
		assert other isa Point3d[Float]
		var result = new Point3d[Float]
		result.x = self.x - other.x
		result.y = self.y - other.y
		return result
	end

	fun *(other: Float): Point3d[N]
	do
		assert self isa Point3d[Float]
		var result = new Point3d[Float]
		result.x = other * self.x
		result.y = other * self.y
		return result
	end

	fun rotate(angle: Float)
	do
		self.x = self.x * angle.cos - self.y * angle.sin
		self.y = self.x * angle.sin + self.y * angle.cos
	end

	fun scalar_product(other: Point3d[Float]): Float
	do
		assert self isa Point3d[Float]
		return self.x * other.x + self.y * other.y
	end

	fun reflect(normal: Point3d[Float])
	do
		assert self isa Point3d[Float]
		var result = self - normal * (2.0 * scalar_product(normal) / normal.length2)
		x = result.x
		y = result.y
	end
end

fun sphere_collide(p1, p2: Point3d[Float], r1, r2: Float): Bool
do
	return p1.dist(p2) <= r1 + r2
end
