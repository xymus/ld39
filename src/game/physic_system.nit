import game_core

# Gravitational constant
fun g: Float
do
	return 0.000066740831
end

redef class Ship
	# Hit `o2`, rebound by default, return `true` if destroyed
	fun rebound(o2: PhysicsObject): Bool
	do
		var o1 = self
		var center_rel = o1.center - o2.center
		var velocity_rel = o1.velocity - o2.velocity
		var total_mass = o1.mass + o2.mass
		var weighted_velocity = (o1.velocity * o1.mass + o2.velocity * o2.mass) * (1.0 / total_mass)
		velocity_rel.reflect(center_rel)
		o1.velocity.x = weighted_velocity.x + velocity_rel.x * (o2.mass / total_mass)
		o1.velocity.y = weighted_velocity.y + velocity_rel.y * (o2.mass / total_mass)
		o2.velocity.x = weighted_velocity.x - velocity_rel.x * (o1.mass / total_mass)
		o2.velocity.y = weighted_velocity.y - velocity_rel.y * (o1.mass / total_mass)

		return false
	end
end

class GravitySystem
	var objects = new Array[PhysicsObject]

	fun remove_object(o: PhysicsObject)
	do
		objects.remove o
	end

	fun force_between_objects(o1, o2: PhysicsObject): Float
	do
		return g * o1.mass * o2.mass / o1.center.dist2(o2.center)
	end

	# Update all `objects` in the system for the last `dt` seconds
	fun update(dt: Float)
	do

		# Collisions
		var objects_to_destroy = new Set[PhysicsObject]
		for i in [0..objects.length[ do
			for j in [i+1..objects.length[ do
				var o1 = objects[i]
				var o2 = objects[j]
				if sphere_collide(o1.center, o2.center, o1.radius, o2.radius) then
					if o1 isa Ship then
						if o1.rebound(o2) then objects_to_destroy.add o1
					else if o2 isa Ship then
						if o2.rebound(o1) then objects_to_destroy.add o2
					else
						if o1.mass <= o2.mass then
							objects_to_destroy.add o1
						end
						if o2.mass <= o1.mass then
							objects_to_destroy.add o2
						end
					end
				end
			end
		end
		for o in objects_to_destroy do
			remove_object o
			o.destroy
		end

		# Gravitational force
		for o1 in objects do
			if not o1.pinned then
				var force = new Point3d[Float]
				for o2 in objects do
					if o1 != o2 then
						var dir = o2.center - o1.center
						dir.normalize
						force += dir * force_between_objects(o1, o2)
					end
				end
				o1.velocity.x += force.x / o1.mass * dt
				o1.velocity.y += force.y / o1.mass * dt
			end
		end

		# Angular acceleration
		for o in objects do
			o.velocity.rotate(o.angular_acceleration * dt)
		end

		# Boost
		for o in objects do
			var prop = 1.0 + o.propulsion * dt
			if prop != 0.0 then
				o.velocity.x *= prop
				o.velocity.y *= prop
			end
		end

		# Update positions
		for o in objects do
			o.center.x += o.velocity.x * dt
			o.center.y += o.velocity.y * dt
		end
	end
end
