import game_core
import physic_system

class Planet
	super PhysicsObject
end

class Asteroid
	super PhysicsObject
end

class Comet
	super PhysicsObject

	# Current system, use for cute effects
	#
	# TODO move up if other objects need this
	var system: nullable SolarSystem = null
end

# A solar system, with one or more stars
class SolarSystem
	super GravitySystem

	# Approximate center of the system, usually the starting position of the star
	var center: Point3d[Float]

	# Orientation
	var counterclockwise = true

	# Ellipse parameters
	var x_radius = 500.0
	var y_radius = 500.0
	fun radius: Float do return x_radius.max(y_radius) end

	# Is `position` in this system?
	fun contains(position: Point3d[Float]): Bool
	do
		var dx = center.x - position.x
		var dy = center.y - position.y
		return (dx * dx) / (x_radius * x_radius) + (dy * dy) / (y_radius * y_radius) <= 1.0
	end

	# Position of the limit at `angle` around `center`?
	#
	# Used to build a visible system limit and place ship in system when entering.
	fun limit_at_angle(angle: Float): Point3d[Float]
	do
		return center.offset(x_radius * angle.cos, y_radius * angle.sin, 0.0)
	end

	# Stars in the system
	var stars = new Array[Star]

	# Add `object` to this system, sorting it in `objects`, `stars` and more
	fun add(object: PhysicsObject)
	do
		objects.add object
		if object isa Star then stars.add object
		if object isa Comet then object.system = self
	end

	fun add_body(body: PhysicsObject, radius, angle, sun_mass: Float)
	do
		body.center.x = center.x + radius * angle.cos
		body.center.y = center.y + radius * angle.sin
		if counterclockwise then
			body.velocity.x = center.y - body.center.y
			body.velocity.y = body.center.x - center.x
		else
			body.velocity.x = body.center.y - center.y
			body.velocity.y = center.x - body.center.x
		end
		var length = body.velocity.length
		body.velocity.normalize
		body.velocity.x *= ((g * sun_mass) / length).sqrt
		body.velocity.y *= ((g * sun_mass) / length).sqrt
		add body
	end
end

fun one_sun_system(center: Point3d[Float]): SolarSystem
do
	var system = new SolarSystem(center)
	system.counterclockwise = 2.rand == 0
	system.x_radius = 400.0 + 100.0.rand
	system.y_radius = 300.0 + 50.0.rand

	# Sun
	var sun = new PinnedStar(1000000000.0, 30.0)
	sun.center.x = center.x
	sun.center.y = center.y
	system.add sun

	# Planets
	var num_planets
	if on_mobile then
		num_planets = 2 + 4.rand
	else
		num_planets = 2 + 8.rand
	end
	for i in [0..num_planets[ do
		var radius = (0.2 + 0.5.rand) * system.x_radius
		var planet = new Planet(radius * 8.0 + 500.0.rand, radius * 0.04 + 15.0.rand)
		var angle = (2.0 * pi).rand
		system.add_body(planet, radius, angle, sun.mass)
	end

	# Asteroids
	var num_asteroids_groups
	if on_mobile then
		num_asteroids_groups = 0 + 2.rand
	else
		num_asteroids_groups = 1 + 2.rand
	end
	for i in [0..num_asteroids_groups[ do
		var group_radius = 40.0 + 250.0.rand
		var group_angle = 2.0 * pi.rand
		var num_asteroids
		if on_mobile then
			num_asteroids = 5 + 5.rand
		else
			num_asteroids = 10 + 10.rand
		end
		for j in [0..num_asteroids[ do
			var radius = group_radius + 25.0.rand
			var asteroid = new Asteroid(10.0 + 10.0.rand, 1.0 + 3.0.rand)
			var angle = group_angle + (1.0 * pi).rand
			system.add_body(asteroid, radius, angle, sun.mass)
		end
	end

	return system
end

fun three_pinned_suns_system(center: Point3d[Float]): SolarSystem
do
	var system = new SolarSystem(center)
	system.x_radius = 500.0
	system.y_radius = 500.0

	var sun1 = new PinnedStar(100000000.0, 32.0)
	sun1.center.x = center.x - 200.0
	sun1.center.y = center.y - 100.0
	system.add sun1

	var sun2 = new PinnedStar(1000000000.0, 32.0)
	sun2.center.x = center.x + 200.0
	sun2.center.y = center.y - 100.0
	system.add sun2

	var sun3 = new PinnedStar(1500000000.0, 32.0)
	sun3.center.x = center.x
	sun3.center.y = center.y + 200.0
	system.add sun3

	return system
end

fun two_moving_suns_system(center: Point3d[Float]): SolarSystem
do
	var system = new SolarSystem(center)
	system.counterclockwise = false
	system.x_radius = 300.0 + 100.0.rand
	system.y_radius = 300.0 + 100.0.rand

	var sun1 = new Star(1000000000.0, 32.0)
	sun1.center.x = center.x - 100.0
	sun1.center.y = center.y - 100.0
	sun1.velocity.y = 10.0
	system.add sun1

	var sun2 = new Star(1000000000.0, 32.0)
	sun2.center.x = center.x + 100.0
	sun2.center.y = center.y - 100.0
	sun2.velocity.y = -10.0
	system.add sun2

	return system
end

# Generate a new solar system
fun generate_system(center: Point3d[Float]): SolarSystem
do
	var n = 100.rand
	if n == 0 then
		return three_pinned_suns_system(center)
	else if n < 20 then
		return two_moving_suns_system(center)
	else
		return one_sun_system(center)
	end
end
