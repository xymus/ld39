import physic_system
import power

redef class Ship
	# Shield strength, in [0.0 .. 1.0]
	var shield = 0.5

	var power_to_shield_ratio = 5.0

	redef fun rebound(other)
	do
		var hit = 0.2
		if other isa Asteroid then
			hit = 0.1
		else if other isa Planet then
			hit = 0.4
		else if other isa Star then
			hit = 0.8
		end

		shield -= hit
		shield = shield.clamp(0.0, 1.0)
		if shield == 0.0 then return true

		return super
	end

	redef fun power_overflow(over) do shield = (shield + over).clamp(0.0, 1.0)
end
