module ld39 is
	app_name "Spacious Righteous Starship"
	app_namespace "org.gamnit.src"
	app_version(0, 1, git_revision)
	android_api_target 10
	android_manifest_activity """android:screenOrientation="sensorLandscape""""
end

import gamnit::flat

import game

import client::client

redef class App

	redef var view = null

	redef fun on_create
	do
		super

		show_splash_screen splash_texture

		# Print errors on textures
		for tex in all_root_textures do
			var error = tex.error
			if error != null then print_error "Texture '{tex}' failed to load: {error}"
		end

		# Title screen "Press any key to start"
		var view = new SplashView
		view.create
		self.view = view

		# Finish setting up world view
		world_camera.reset_height 300.0
	end

	redef fun start_new_game
	do
		var view = view
		if view != null then view.destroy

		status_text.text = null

		# Setup the player's ship and game
		var ship = new Ship(4.0, 2.0)
		var game = new Game(ship)

		# Create the first few solar systems
		game.explore new Point3d[Float]

		# Add `ship` to a random system
		var system = null
		for s in game.solar_systems do
			system = s
			if s.stars.length == 1 then break
		end
		assert system != null

		var r = 0.5 * system.radius
		system.add_body(ship, r, 0.0, system.stars.first.mass)
		ship.add_to_scene

		app.world_camera.position.x = ship.center.x
		app.world_camera.position.y = ship.center.y

		view = new PlayView(game, system)
		view.create
		self.view = view

		system.objects.remove ship
		view.enter_system system
	end

	redef fun update(dt)
	do
		super

		var view = view
		if view != null then view.update(dt)
	end

	redef fun accept_event(event)
	do
		if super then return true

		if event isa QuitEvent or
		  (event isa KeyEvent and event.name == "escape" and event.is_up) then
			# When window close button, escape or back key is pressed
			print "Ran at {current_fps} FPS in the last few seconds"

			print "Performance statistics to detect bottlenecks:"
			print sys.perfs

			# Quit abruptly
			exit 0
			return true
		end

		var view = view
		if view != null then return view.accept_event(event)

		return false
	end
end

redef class Ship
	redef fun destroy
	do
		super

		var view = new DeathView(app.view.as(PlayView))
		view.create
		app.view = view
	end
end
