Spacious Righteous Starship

# Authors

Alexis Laferriere and Alexandre Blondin Massé

# Art

* Remote explosion sound created by NenadSimic under CC0.
* Close explosion sound created by dklon under CC-BY 3.0.
* Original jetpack sound created by Cambra, published under CC BY 3.0.
